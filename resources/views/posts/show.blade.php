@extends('layouts.app')
@section('title',$post->title)

@section('content')
<div class="card">
    <div class="card-body">
        <h2>{{$post->title}}</h2>
        <p>{{$post->description}}</p>
        <p><strong> Цена :{{$post->price}}$</strong></p>
    </div>
</div>
@endsection
