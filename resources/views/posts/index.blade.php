@extends('layouts.app')
@section('title', 'Все посты')
@section('content')
<body>
<a href="{{route('posts.create')}}"class="btn btn-success">Добавить новый товар</a>
@if(session()->get('success'))
<div class="alert alert-success mt-3">
  {{session()->get('success')}}
</div>
@endif
<table id="data_table" class="table table-striped table-dark mt-3">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Наименование</th>
      <th scope="col">Описание</th>
      <th scope="col">Цена</th>
      <th style="text-align: center;" scope="col">Действия</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
      @foreach($posts as $post)
    <tr>
      <th scope="row">{{$post->id}}</th>
      <td>{{$post->title}}</td>
      <td>{{$post->description}}</td>
      <td>{{$post->price}}</td>
      <td class="table-buttons">
          <a href="{{ route('posts.show', $post) }}"class="btn btn-success">
          <i class="fa fa-eye"></i>
          </a>
          <a href="{{ route('posts.edit',$post) }}" class="btn btn-primary">
          <i class="fa fa-pencil"></i>
          </a>
            @csrf
            @method('DELETE')
          <button data-id="{{ $post->id }}"  type="submit" class="btn btn-danger" id="deleteProduct"> 
          <i class="fa fa-trash" ></i>
          </button>
      </td>
    </tr>
    @endforeach     
  </tbody>
</table>
<script>
$(document).ready(function () {

$("body").on("click","#deleteProduct",function(e){

   if(!confirm("Вы действительно хотите удалить?")) {
      return false;
    }

   e.preventDefault();
    var id = $(this).attr('data-id');
   var token = $("meta[name='csrf-token']").attr("content");
   var url = e.target;

   $.ajax(
       {
         url:"posts/"+id, 
         type: 'DELETE',
         data: {
           _token: token,
               id: id
       },
       success: function (response){
           $("#success").html(response.message)

           Swal.fire(
             'Good!',
             'Product deleted successfully!',
             'success'
           )
           location:reload();
       }
    });
     return false;
  });
  
});
</script>
</body>
@endsection
