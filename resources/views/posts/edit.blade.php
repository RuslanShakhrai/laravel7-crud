@extends('layouts.app')
@section('title',$post->title)

@section('content')
<div class="row">
<div class="col-lg-6 mx-auto">
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="POST" action="{{ route('posts.update',$post) }}">
  @csrf
  @method('PATCH')
  <div class="form-group">
    <label for="item-title">Наименование товара</label>
    <input type="text" value="{{$post->title}}" class="form-control" id="title" placeholder="Apple IPHONE X" name="title" >
  </div>
  <div class="form-group">
    <label for="item-description">Описание товара</label>
    <textarea name="description" class="form-control" id="description" rows="3" >{{$post->description}}</textarea>
  </div>
  <div class="form-group">
    <label for="item-price">Цена за единицу товара</label>
    <input type="number"value="{{$post->price}}" class="form-control" id="price" placeholder="X $" name="price" >
  </div>
  <button type="submit"class="btn btn-success">Отредактировать</button>
  </div>
  </div>
</form>
@endsection
