@extends('layouts.app')
@section('title', 'Добавить новый товар')

@section('content')
<div class="row">
<div class="col-lg-6 mx-auto">
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<form method="POST" action="{{ route('posts.store') }}">
  @csrf
  <div class="form-group">
    <label for="item-title">Наименование товара</label>
    <input type="text" value="{{old('title')}}" class="form-control" id="title" placeholder="Apple IPHONE X" name="title" >
  </div>
  <div class="form-group">
    <label for="item-description">Описание товара</label>
    <textarea name="description" class="form-control" id="description" rows="3" >{{old('description')}}</textarea>
  </div>
  <div class="form-group">
    <label for="item-price">Цена за единицу товара</label>
    <input type="number"value="{{old('price')}}" class="form-control" id="price" placeholder="X $" name="price" >
  </div>
  <button type="submit"class="btn btn-success">Добавить товар</button>
  </div>
  </div>
</form>
@endsection
