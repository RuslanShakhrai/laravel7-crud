<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=Post::all();
        return view('posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255',
            'price'=> 'required||max:11'
        ]);
        $post=new Post([
            'title'=>$request->get(htmlspecialchars(trim('title'))),
            'description'=>$request->get(htmlspecialchars(trim('description'))),
            'price'=>$request->get(htmlspecialchars(trim('price'))),

        ]);
        $post->save();
        return redirect('/posts')->with('success','Товар был удачно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::find($id);
        
        return view('posts.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::find($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255',
            'price'=> 'required||max:11'
        ]);
        $post=Post::find($id);
        $post->title=$request->get('title');
        $post->description=$request->get('description');
        $post->price=$request->get('price');
        $post->save();

        return redirect('/posts')->with('success','Данные о товаре были успешно отредактированы!');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $post=Post::find($id);
    //     $post->delete();

    //     
    // }
    public function destroy($id){
        $post = Post::find($id);
        $post->delete();
         return response()->json([
          'message' => 'Data deleted successfully!'
         ]);
        //return redirect('/posts')->with('success','Товар был успешно удален!');

    }
}
